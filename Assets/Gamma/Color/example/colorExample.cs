﻿using UnityEngine;
using System.Collections;
using GammaColor;
using UnityEngine.UI;

public class colorExample : MonoBehaviour {

	public Image colorImage;
	public InputField inputField;
	public Text colorTextLabel;

	// Use this for initialization
	public void ApplyColor () {
		colorImage.color = inputField.text.ToRGBAColor();

		HSVAColor hsvColor = colorImage.color.ToHSVA();

		colorTextLabel.text = string.Format("Hue: {0}\nSaturation: {1}\nValue: {2}\nAlpha: {3}", hsvColor.h, hsvColor.s, hsvColor.v, hsvColor.a);
	}


}
