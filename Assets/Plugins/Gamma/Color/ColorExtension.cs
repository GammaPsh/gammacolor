﻿using UnityEngine;
using System.Collections;

namespace Gamma.Color {
	public static class ColorExtension {
		
		public static string ToHexRGBA(this Color color)
		{
			return "#"+ ((int)(color.r*255)).ToString("X2")+((int)(color.g*255)).ToString("X2")+((int)(color.b*255)).ToString("X2")+((int)(color.a*255)).ToString("X2");
		}

		public static string ToHexRGB(this Color color)
		{
			return "#"+ ((int)(color.r*255)).ToString("X2")+((int)(color.g*255)).ToString("X2")+((int)(color.b*255)).ToString("X2");
		}
		
		public static Color FromHexRGBA(this Color color, string hexstring)
		{
			return hexstring.ToRGBAColor();
		}

		public static Color FromHexRGB(this Color color, string hexstring)
		{
			return hexstring.ToRGBAColor();
		}

		public static HSVAColor ToHSVA(this Color color)
		{
			return new HSVAColor(color);
		}

		public static Color ToRGBAColor(this string hexstring)
		{

			if(hexstring[0] == '#'){
				hexstring = hexstring.Remove(0, 1);
			}

			int r = 255;
			int g = 255;
			int b = 255;
			int a = 255;
			try {
				r = System.Convert.ToInt32(hexstring.Substring(0, 2), 16);
				g = System.Convert.ToInt32(hexstring.Substring(2, 2), 16);
				b = System.Convert.ToInt32(hexstring.Substring(4, 2), 16);
				if(hexstring.Length == 8){
					a = System.Convert.ToInt32(hexstring.Substring(6, 2), 16);
				}
			}catch{
				throw new System.ArgumentException("Given string was not a valid RGB or RGBA formated hex string");
			}

			return new UnityEngine.Color((float)r/255f,(float)g/255f,(float)b/255f,(float)a/255f);
		}
	}
}
