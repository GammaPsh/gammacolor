
using UnityEngine;
using System.Collections;

namespace Gamma.Color {
	public class HSVAColor {
		
		public float h;
		public float s;
		public float v;
		public float a;
		
		public HSVAColor(float h, float s, float v, float a)
		{
			this.h = h;
			this.s = s;
			this.v = v;
			this.a = a;
		}
		
		public HSVAColor(Color rgbaColor)
		{
			this.a = rgbaColor.a;

			int largest = 0;
			float Min;
			float Max;
			
			if(rgbaColor.r <= rgbaColor.g){
				Min = Mathf.Clamp01(rgbaColor.r);
				Max = Mathf.Clamp01(rgbaColor.g);
				largest = 2;
				if(Min > rgbaColor.b){
					Min = Mathf.Clamp01(rgbaColor.b);
				}
				if(Max < rgbaColor.b){
					Max = Mathf.Clamp01(rgbaColor.b);
					largest = 3;
				}
			}else{
				Min = Mathf.Clamp01(rgbaColor.g);
				Max = Mathf.Clamp01(rgbaColor.r);
				largest = 1;
				if(Min > rgbaColor.b){
					Min = Mathf.Clamp01(rgbaColor.b);
				}
				if(Max < rgbaColor.b){
					Max = Mathf.Clamp01(rgbaColor.b);
					largest = 3;
				}
			}
			
			if(Max == Min){
				if(Max == 0){
					h = 0;
					s = 0;
					v = 0;
					return;
				}
				h = 0;
				s = (Max-Min)/Max;
				v = Mathf.Max(Max, 0.1f);
				return;
			}
			s = (Max-Min)/Max;
			v = Max;
			
			float _h;
			switch(largest){
			case 1:
				_h = 60.0f*((rgbaColor.g-rgbaColor.b)/(Max-Min));
				h = _h > 0 ? _h : _h+360.0f;
				return;
			case 2:
				_h = 60.0f*(2.0f+(rgbaColor.b-rgbaColor.r)/(Max-Min));
				h = _h > 0 ? _h : _h+360.0f;
				return;
			case 3:
				_h = 60.0f*(4.0f+(rgbaColor.r-rgbaColor.g)/(Max-Min));
				h = _h > 0 ? _h : _h+360.0f;
				return;
			}
			throw new System.NotSupportedException("HSV From Unity Color failed");
		}
		
		public HSVAColor(){
		}
		
		public Color ToRGBA(){
			
			int Hi = Mathf.FloorToInt(h/60);
			float f = h/60.0f - Hi;
			
			switch(Hi){
			case 1:
				//q,v,p
				return new Color(v*(1-s*f), v, v*(1-s), a);
			case 2:
				//p,v,t
				return new Color(v*(1-s), v, v*(1-s*(1-f)), a);
			case 3:
				//p,q,v
				return new Color(v*(1-s), v*(1-s*f), v, a);
			case 4:
				//t,p,v
				return new Color(v*(1-s*(1-f)), v*(1-s), v, a);
			case 5:
				//v,p,q
				return new Color(v, v*(1-s), v*(1-s*f), a);
			default:
				//v,t,p
				return new Color(v, v*(1-s*(1-f)), v*(1-s), a);
			}
		}
		
		public override string ToString(){
			return "HSVA:(" + h.ToString("0.#") + ", " + s.ToString("0.###") +", "+ v.ToString("0.###") +", "+ a.ToString("0.###") +")";
		}
	}
}
